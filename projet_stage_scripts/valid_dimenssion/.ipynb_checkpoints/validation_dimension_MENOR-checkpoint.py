#!/usr/bin/env python
# coding: utf-8

# In[2]:


import os
import numpy as np
import xarray as xr
import pandas as pd
from datetime import datetime
from datetime import timedelta
import glob

# packages for plotting
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib.colors import ListedColormap
import seaborn as sns

from configobj import ConfigObj
import netCDF4 as nc


# ### Créer les paramètres (changer si MANGAE et si MENOR)


config = ConfigObj("/home7/datahome/marc/lib_operational_chain/data_oco/config_MARC_F2-MARS3D-MENOR1200.cfg")

#dt debut
startDate=config['config']['model_avaibility_start_date']

int(config['config']['echeances'])/24

#dt fin
endDate = datetime.now()+timedelta(days=int(config['config']['echeances'])/24)  # aujourd'hui + 4 jours de forecasting
endDate = endDate.strftime("%Y%m%d")

#frequence
frequence = config['config']['frequency'] + 'H' # MANGAE2500=3h ,MENOR1200=1h

#repertoire
refDir = config['config']['oco_dir_best']

#filename
filePrefix = config['config']['oco_name']

#dimensions
dim_ni = int(config['config']['dim_ni'])
dim_nj = int(config['config']['dim_nj'])
dim_level = int(config['config']['dim_level'])
dim_time = int(config['config']['dim_time'])
nb_vars = int(config['config']['nb_vars'])


# In[15]:


config


# # Vérification des fichier

# ### Créer les data frames de dates (attendus et vrais)

# In[4]:


#all_files=glob.glob(os.path.join(refDir,"2006",filePrefix+'_'+"200601*T*Z.nc"))

all_files = glob.glob(os.path.join(refDir,"*",filePrefix+'_'+'*Z'+config['config']['suffix']+".nc"))

li=[]
for i in  all_files:
    ds= xr.open_dataset(i)
    files_with_size =  (datetime.strptime(os.path.basename(i), filePrefix+'_'+config['config']['date_format']+".nc"),os.stat(i).st_size/ 1024 ** 2,
                        pd.to_datetime(ds.time.values[0]), ds.dims['time'],ds.dims['level'],ds.dims['ni'],ds.dims['nj'],len(ds))
    li.append(files_with_size)
    print(li[-1][0])
df_dt_real = pd.DataFrame(li, columns =['dt_fichier','taille_fichier','time', 'dim_time','dim_level','dim_ni', 'dim_nj','nombre_var'])
df_dt_real['time_egal'] = np.where(df_dt_real['dt_fichier']==df_dt_real['dt_fichier'], 0,1)

#dimension conforme -0 =conforme , 1= nconforme
df_dt_real['dim_n_conforme'] = np.where((df_dt_real['time_egal']==0) & 
                                                      (df_dt_real['dim_time']==dim_time) & (df_dt_real['dim_level']==dim_level) & (df_dt_real['dim_ni']==dim_ni)  &
                                                      (df_dt_real['dim_nj']==dim_nj) & (df_dt_real['nombre_var']==nb_vars) ,0, 1)

df_dt_real.sort_values(by='dt_fichier',ignore_index=True)      


# In[6]:


df_dt_attendu = pd.DataFrame( pd.date_range(start=startDate, end=endDate, freq=frequence),columns=['dt_attendu'])


# ### Créer le df complet
# 

result = pd.merge(df_dt_attendu, df_dt_real, how="left", left_on="dt_attendu" , right_on = "dt_fichier").fillna(0)
result['annee'] = result['dt_attendu'].apply(lambda x: x.year).fillna(0)
result['mois'] = result['dt_attendu'].dt.month_name() 
#result['mois'] = result['dt_attendu'].dt.month_name(locale = 'French') 
result['annee_mois'] =result["dt_attendu"].dt.to_period('M').dt.strftime('%Y-%m')
result['manquante'] = np.where(result['dt_fichier']==0, 1,0)
result


# ### Représentation graphique -  taille des fichiers

# In[8]:


#group by taille
result_taille=result.groupby(['annee','annee_mois'], as_index=False)[['annee_mois','taille_fichier']].mean()

import matplotlib.pyplot as plt
import seaborn as sns
plt.style.use('seaborn')
sns.set_theme(style='white')  # set background color
fig, ax = plt.subplots(figsize=(70,10))
plt.bar(result_taille['annee_mois'], result_taille['taille_fichier'], color='gray' ,width=0.3)
plt.xticks(rotation=50, horizontalalignment="center") 
ax.set_xlabel('Mois', fontsize=20, fontweight='black', color = '#333F4B')
ax.set_ylabel('Taille moyenne (M)', fontsize=20, fontweight='black', color = '#333F4B')
plt.title('TAILLE MOYENNE DES FICHIERS'+ ' ' + config['config']['oco_name'] , fontsize=25 )
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
plt.savefig("validation_fichiers/Valid_taille_moyenne_fichiers_"+config['config']['oco_name']+".png", dpi=40, bbox_inches='tight')
plt.show(block=True);


# ### Représentation graphique - données manquantes par année

# In[32]:


#group by données manquants par année
result_manq=result.groupby(['annee','manquante'], as_index=False)[['annee_mois','manquante']].sum()
  
plt.style.use('seaborn')
sns.set_theme(style='white')  # set background color
fig, ax = plt.subplots(figsize=(40,10))

plt.bar(result_manq['annee'], result_manq['manquante'], color='red' ,width=0.1)
plt.xticks(rotation=50, horizontalalignment="center") 
ax.set_xlabel('Année', fontsize=20, fontweight='black', color = '#333F4B')
ax.set_ylabel('Nombre de fichiers manquants', fontsize=20, fontweight='black', color = '#333F4B')
plt.title('FICHIERS MANQUANTS PAR ANNÉE'+ ' ' + config['config']['oco_name'] , fontsize=25)


ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
    
plt.savefig("validation_fichiers/menor/Valid_manquants_par_annee_"+config['config']['oco_name']+".png", dpi=40, bbox_inches='tight')
plt.show(block=True);


# ### Représentation graphique - données manquantes par mois
# liste des années manquantes
mylist =pd.Series(list(dict.fromkeys(result[(result.manquante==1.0)].annee)),name='annee')

# merge pour creer la liste seulement des années où il y a des fichiers manquantes
result2= result.merge(mylist,how='inner' )

result_manq_mois=result2.groupby(['annee','annee_mois','manquante'], as_index=False)[['manquante']].sum()
 
plt.style.use('seaborn')
sns.set_theme(style='white')  # set background color
fig, ax = plt.subplots(figsize=(70,10))

plt.bar(result_manq_mois['annee_mois'], result_manq_mois['manquante'], color='red' ,width=0.1)
plt.xticks(rotation=50, horizontalalignment="center") 
ax.set_xlabel('Mois', fontsize=20, fontweight='black', color = '#333F4B')
ax.set_ylabel('Nombre de fichiers manquants', fontsize=20, fontweight='black', color = '#333F4B')
plt.title('FICHIERS MANQUANTS PAR MOIS'+ ' ' + config['config']['oco_name'] , fontsize=25)

ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
    
plt.savefig("validation_fichiers/menor/Valid_manquants_par_mois_"+config['config']['oco_name']+".png", dpi=40, bbox_inches='tight')
plt.show(block=True);
# ### Représentation graphique -  dimensions n conformees   
#group by des dimensions n conformes
result_dim_nconf=result.groupby(['annee','annee_mois'], as_index=False)[['annee_mois','dim_n_conforme']].sum()

plt.style.use('seaborn')
sns.set_theme(style='white')  # set background color
fig, ax = plt.subplots(figsize=(70,10))

plt.bar(result_dim_nconf['annee_mois'], result_dim_nconf['dim_n_conforme'], color='red' ,width=0.2)
plt.xticks(rotation=50, horizontalalignment="center") 
ax.set_xlabel('Mois', fontsize=20, fontweight='black', color = '#333F4B')
ax.set_ylabel('Nombre de fichiers n conforms', fontsize=20, fontweight='black', color = '#333F4B')
plt.title('DIMENSIONS NON CONFORMES'+ ' ' + config['config']['oco_name'] , fontsize=25)

ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.savefig("validation_fichiers/menor/Valid_dimensions_n_conf_"+config['config']['oco_name']+".png", dpi=40, bbox_inches='tight')
plt.show(block=True);
# ### Sauvegarder les dates manquantes et non conforms dans un fichier.scv
# manquantes:
result[(result.manquante==1.0)].dt_attendu.to_frame().reset_index(drop=True).rename(columns = {'dt_attendu':'dt_manquantes'}).to_csv("validation_fichiers/Valid_manquants_par_mois_"+config['config']['oco_name']+".csv" ,sep=',', encoding='utf-8', index=False) 
#n conformes:
result[(result.dim_n_conforme==1.0)].reset_index(drop=True).to_csv("validation_fichiers/menor/Valid_dimensions_n_conf_"+config['config']['oco_name']+".csv",sep=',', encoding='utf-8', index=False) 


# In[ ]:




