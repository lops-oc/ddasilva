#!/usr/bin/env python
# coding: utf-8

# # Générer le fichier avec XE, SLA, Masque

# ### Import library

# In[1]:


import intake
import dask_hpcconfig
from distributed import Client
import xarray as xr
import hvplot.xarray
import geoviews.feature as gf
import os
import glob
import os
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats 
import seaborn as sns
import numpy as np
import matplotlib
from py_eddy_tracker.poly import create_vertice

# cartopy
from cartopy import config
import cartopy.crs as ccrs

# eddy tracker
from datetime import datetime
from matplotlib import pyplot as plt
from py_eddy_tracker import data
from py_eddy_tracker.dataset.grid import RegularGridDataset

from py_eddy_tracker import data
from py_eddy_tracker.observations.observation import EddiesObservations
from matplotlib import pyplot as plt
from numpy import arange, cos, linspace, radians, sin
from py_eddy_tracker.eddy_feature import Contours
from py_eddy_tracker.generic import local_to_coordinates
from datetime import timedelta, date
import os, glob


# # Dask

# In[ ]:


overrides = {"cluster.cores": 7,"cluster.n_workers":7,"cluster.processes":7}
cluster = dask_hpcconfig.cluster("datarmor", **overrides)
cluster.scale(jobs=4)
client = Client(cluster)


# In[ ]:


client
# # Paramètres

# In[ ]:


period='2010_2021'
region='MENOR'

# In[ ]:

#ds_total = xr.open_mfdataset("/home1/datawork/ddasilva/data/sla_masque/*.nc")
#path="/home1/datawork/ddasilva/data/fichiers_finaux/FICHIERS_"+region+"_"+period+".nc" 
#ds_total.to_netcdf(path=path) 

files = f"/home1/scratch/jfleroux/SLA_MASQUE_MENOR_*.nc"
ds = xr.open_mfdataset(files,combine='nested',concat_dim="time")

path="/home1/scratch/ddasilva/FICHIERS_"+region+"_"+period+".nc" 
ds.to_netcdf(path=path) 