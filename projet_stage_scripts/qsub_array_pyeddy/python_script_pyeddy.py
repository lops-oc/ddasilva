import sys
import intake
import dask_hpcconfig
from distributed import Client
import xarray as xr
import hvplot.xarray
import geoviews.feature as gf
import os
import glob
import os
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats 
import seaborn as sns
import numpy as np
import matplotlib
from math import *
from py_eddy_tracker.poly import create_vertice

# cartopy
from cartopy import config
import cartopy.crs as ccrs

# eddy tracker
from datetime import datetime
from matplotlib import pyplot as plt
from py_eddy_tracker import data
from py_eddy_tracker.dataset.grid import RegularGridDataset

from py_eddy_tracker import data
from py_eddy_tracker.observations.observation import EddiesObservations
from matplotlib import pyplot as plt
from numpy import arange, cos, linspace, radians, sin
from py_eddy_tracker.eddy_feature import Contours
from py_eddy_tracker.generic import local_to_coordinates
from datetime import timedelta, date
import os, glob

# Dask
#overrides = {"cluster.cores": 7,"cluster.n_workers":7,"cluster.processes":7}
#cluster = dask_hpcconfig.cluster("datarmor", **overrides)
#cluster.scale(jobs=4)
#client = Client(cluster)
#client

index_tab = int(sys.argv[1])
size_tab = int(sys.argv[2])

print("Parallel tab {0} sur {1}".format(index_tab,size_tab))

ds= sorted(glob.glob(os.path.join("/home1/datawork/ddasilva/data/sla/ANO_MENOR_*.nc")))
 
bloc = ceil(len(ds)/size_tab)
start_index=  bloc*(index_tab-1)
stop_index= bloc*(index_tab) - 1

print("start_index : {} stop_index {}".format(start_index,stop_index))

if (stop_index > len(ds)):
    stop_index = len(ds)

 
    #*********** PY-EDDY-TRACKER********************

     # * Partie 1:
import time
for i in ds[start_index:stop_index+1]:
    dt_str=  os.path.basename(i)[10:20] # OBS !!!: FAIRE AVEC SPLIT FROM '_' TO ".NC"
    dt_hr=datetime.strptime(dt_str, "%Y%m%d%H") 
    g = RegularGridDataset("/home1/datawork/ddasilva/data/sla/ANO_MENOR_"+dt_str+".nc","ni","nj",)  #eddy grid from sla filles
    g.add_uv("SLA")
    u, v = g.grid("u").T, g.grid("v").T
    a, c= g.eddy_identification("SLA", "u", "v", dt_hr, 0.002, shape_error=55) #eddy idenfitication for date 00:00h
    SLA = g.grid("SLA")
    
        # * Partie 2:
        
        #**************** MASK a : anticyclonique ****************
    x_name, y_name = a.intern(False)
    mask_a = np.zeros(SLA.shape, dtype="bool")
    
    # **************** MASK c : cyclonique  ****************
    x_name_c, y_name_c = c.intern(False)
    mask_c = np.zeros(SLA.shape, dtype="bool")

    for eddy in a:
        i, j = matplotlib.path.Path(create_vertice(eddy[x_name], eddy[y_name])).pixels_in(g)
        mask_a[i, j] = True
        SLA.mask[::] += ~mask_a
        mask_a_2=mask_a.astype(int)
       # li_a.append(a)

    for eddy in c:
        i, j = matplotlib.path.Path(create_vertice(eddy[x_name_c], eddy[y_name_c])).pixels_in(g)
        mask_c[i, j] = True
        SLA.mask[::] += ~mask_c
        mask_c_2=2*mask_c
      #  li_c.append(c)
    
    #************ MASC a+c ********************
    mask_ac=np.array(mask_a_2+mask_c_2) #join masks
    mask_ac_2=np.where(mask_ac <=2, mask_ac, 2) # keep just class 0,1 and 2 . if other, value is equal to 2     
    ac=np.dstack(mask_ac_2)

            #  Partie 3: sauvegarder masque avec XE et SLA 

    #********** preparing ANO file ********************
    ds_1 = xr.open_dataset("/home1/datawork/ddasilva/data/sla/ANO_MENOR_"+dt_str+".nc",chunks={'time':100})
    lat = ds_1['nj'].values
    long = ds_1['ni'].values
    time_nv = ds_1['time'].values
    
    #********** save mask ********************
    me = xr.DataArray(ac, coords={'time': time_nv,'nj': lat, 'ni': long},dims=['time','nj', 'ni'])
    ds_1['masque'] = me
    path="/home1/datawork/ddasilva/data/sla_masque/SLA_MASQUE_MENOR_"+dt_str+".nc" 
    ds_1.to_netcdf(path=path) 
