import sys
import intake
import dask_hpcconfig
from distributed import Client
import xarray as xr
import hvplot.xarray
import geoviews.feature as gf
import os
import glob
import os
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import seaborn as sns
import numpy as np
from math import *

# cartopy
from cartopy import config
import cartopy.crs as ccrs

# eddy tracker
from datetime import datetime
from matplotlib import pyplot as plt

index_tab = int(sys.argv[1])
size_tab = int(sys.argv[2])

print("Parallel tab {0} sur {1}".format(index_tab,size_tab))

def list_param(cat, param):
    return cat.metadata["parameters"][param]["allowed"]

def allowed_param(cat):
    return cat.metadata["parameters"]

def allowed_years(data):
    path = "/home/datawork-lops-iaocea/catalog/kerchunk/ref-marc/" + data
    files = [f.removesuffix(".json.zst") for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    files.sort()
    
    return files

def year_concat(cat, source, region, years,chunks):
    concat = [cat[source](region=region, year=i,chunks=chunks).to_dask() 
              for i in years]

    return xr.concat(
        concat, 
        dim="time", 
        coords="minimal", 
        data_vars="minimal", 
        compat="override"
    )
cat = intake.open_catalog("/home/datawork-lops-iaocea/catalog/intake/marc.yaml")

# Parametres

regions = list_param(
    cat, "region"
)
# variable
varname = "XE"

# region
region = 'f2_1200'  #menor -mediteranne - 2010 a 2013
region2 = 'f2_1200_v10'  #menor -mediteranne 2013 et 14
region3 = 'f2_1200_sn'  #menor -mediteranne 2015-2022

year = allowed_years(data=region)
year2 = allowed_years(data=region2)
year3 = allowed_years(data=region3)

# year
year_select=year
year_select2=year2[0:-1]
year_select3=year3[0:-1]

# ds total
ds1 = year_concat(cat, "marc", region,year_select,chunks={ "nj": -1 ,"ni": -1})
ds2 = year_concat(cat, "marc", region2,year_select2,chunks={ "nj": -1 ,"ni": -1})
ds3 = year_concat(cat, "marc", region3,year_select3,chunks={ "nj": -1 ,"ni": -1})

ds1=ds1.sel(time=(slice('2010-01-01T00:00:00.000000000','2013-08-31T00:00:00.000000000')))

ds_monthly  =  ds1[varname]
ds_monthly2 =  ds2[varname]
ds_monthly3 =  ds3[varname]

ds = xr.concat([ds_monthly,ds_monthly2,ds_monthly3],'time')


#CLIM
path="/home1/datawork/ddasilva/data/sla/CLIMAT_2010_2021.nc" 
ds_clim = xr.open_mfdataset(path)

ano = ds.groupby("time.month") - ds_clim
                                                                                                                                                                            
ano.attrs["units"]="m"
ano.units

# convertir les coordennées
ano['ni'] = ano.longitude[0,:].values
ano['nj'] = ano.latitude[:,0].values
# ano=ano.rename("SLA") 
ano

bloc = ceil(len(ds.time)/size_tab)
start_index=  bloc*(index_tab-1)
stop_index= bloc*(index_tab) - 1

print("start_index : {} stop_index {}".format(start_index,stop_index))

if (stop_index > len(ds.time)):
    stop_index = len(ds.time)

wk_ds = ano.isel(time=(slice(start_index,stop_index+1)))
ds_time= wk_ds.time.values

print(ds_time)
for i in ds_time:
    date_time_str=  pd.to_datetime(i).strftime("%Y%m%d%H")
    ds_1 = wk_ds.sel(time=slice(i,i+1))
    path="/home1/datawork/ddasilva/data/sla/ANO_"+date_time_str+".nc" 
    ds_1.to_netcdf(path=path)