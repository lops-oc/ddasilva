#PBS -l mem=10g
#PBS -l walltime=01:00:00

cd $PBS_O_WORKDIR

#qstat -f $PBS_ARRAY_ID

source /usr/share/Modules/3.2.10/init/csh

conda activate pyEddyTracker
which python

date
python ../python_script.py $PBS_ARRAY_INDEX $size > output_${PBS_ARRAY_ID}_${PBS_ARRAY_INDEX}.out
date

