#!/usr/bin/env python
# coding: utf-8

# # EddyRESNet 

# Note : 
# * EddyRESNet est un réseau convolutif UNET + résiduous.
# * Nous avons utilise 4.013 comme données d’entraînement et 365 comme donnes test avec une taille 168x200 .
# * reference : https://github.com/redouanelg/EddyNet

# In[ ]:



import xarray as xr
import hvplot.xarray
import geoviews.feature as gf
import os
import glob
import os
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats 
import seaborn as sns
import numpy as np
import matplotlib
from py_eddy_tracker.poly import create_vertice

# cartopy
from cartopy import config
import cartopy.crs as ccrs

# eddy tracker
from datetime import datetime
from matplotlib import pyplot as plt
from py_eddy_tracker import data
from py_eddy_tracker.dataset.grid import RegularGridDataset

from py_eddy_tracker import data
from py_eddy_tracker.observations.observation import EddiesObservations
from matplotlib import pyplot as plt
from numpy import arange, cos, linspace, radians, sin
from py_eddy_tracker.eddy_feature import Contours
from py_eddy_tracker.generic import local_to_coordinates
from datetime import timedelta, date
import os, glob


# In[1]:


from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "all"
# to output everything in a cell instead of only the last output

get_ipython().run_line_magic('matplotlib', 'inline')

# standard imports
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt


# ###  import data
# 
# * expand_dims: the expand dim because Keras needs for training the following dims (nb of training, height, width, channels) 

# In[ ]:


SSH_train = np.expand_dims(np.load('/home1/datahome/ddasilva/stage/fichiers_inputs/XE_train_orig_2010_2020_f2_1200_sn.npy' ),3)[:,0:168,100:300,:] #you can use all the region, in here I selected a box of 168*168 size
SSH_test = np.expand_dims(np.load('/home1/datahome/ddasilva/stage/fichiers_inputs/XE_test_orig_2021_f2_1200_sn.npy'),3)[:,0:168,100:300,:] 
#######
Seg_train = np.expand_dims(np.load('/home1/datahome/ddasilva/stage/fichiers_inputs/SLA_train_segm_2010_2020_f2_1200_sn.npy'),3)[:,0:168,100:300,:]
Seg_test = np.expand_dims(np.load('/home1/datahome/ddasilva/stage/fichiers_inputs/SLA_test_segm_2021_f2_1200_sn.npy'),3)[:,0:168,100:300,:]
######
SSH_train.shape
SSH_test.shape
#####
Seg_train.shape
Seg_test.shape


# In[ ]:


randindex=np.random.randint(0,len(SSH_train))

plt.figure(figsize=(20, 10))

plt.subplot(121)
plt.imshow(SSH_train[randindex,:,:,0], cmap='viridis')
plt.colorbar(extend='both', fraction=0.042, pad=0.04)
#plt.clim(-0.25,0.25)
plt.axis('off')
plt.title('SSH');

plt.subplot(122)
plt.imshow(Seg_train[randindex,:,:,0], cmap='viridis')
plt.colorbar(extend='both', fraction=0.042, pad=0.04)
#plt.clim(-0.25,0.25)
plt.axis('off')
plt.title('ground truth Segmentation');


# ### EddyResNet
# This architecture is based on combining Unet and Residual units, here we show a small EddyResNet
# 
# 

# In[3]:


from keras.models import Model, load_model
from keras.layers import Activation, Reshape, Permute, Lambda
from keras.layers import Input, Conv2D, MaxPooling2D, Dropout, UpSampling2D, AlphaDropout, concatenate, Conv2DTranspose
from keras.layers import BatchNormalization, LeakyReLU, add
from keras.utils import np_utils
from tensorflow.keras.optimizers import Adam 
from keras import backend as K
from keras import regularizers
import matplotlib.pyplot as plt
import numpy as np
import pickle
from IPython.display import SVG 
from keras.utils.vis_utils import model_to_dot
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau


# In[ ]:


Seg_train_categor = np_utils.to_categorical(np.reshape(Seg_train[:,:,:,0],(4013,168*200)),3)
Seg_train_categor.shape


# ### U- NET Architecture

# In[5]:


def FirstResUnit(nf,ker,inputs):
    conv1 = Conv2D(nf, ker, padding="same", kernel_initializer='he_normal', use_bias=False)(inputs)
    conv1 = BatchNormalization()(conv1)
    conv1 = Activation('relu')(conv1)
    conv2 = Conv2D(nf, ker, padding="same", kernel_initializer='he_normal', use_bias=False)(conv1)
    return add([conv2,inputs])

def ResUnit(nf,ker,inputs, drop=0.5):
    conv1 = BatchNormalization()(inputs)
    conv1 = Activation('relu')(conv1)
    conv1 = Dropout(drop)(conv1)
    conv2 = Conv2D(nf, ker, padding="same", kernel_initializer='he_normal', use_bias=False)(conv1)
    conv2 = BatchNormalization()(conv2)
    conv2 = Activation('relu')(conv2)
    conv2 = Dropout(drop)(conv2)
    conv2 = Conv2D(nf, ker, padding="same", kernel_initializer='he_normal', use_bias=False)(conv2)
    return add([conv2,inputs])

def ResUnitDecoder(nf,ker,inputs,drop=0.5):
    conv1 = BatchNormalization()(inputs)
    conv1 = Activation('relu')(conv1)
    conv1 = Dropout(drop)(conv1)
    conv2 = Conv2D(nf, ker, padding="same", kernel_initializer='he_normal', use_bias=False)(conv1)
    conv2 = BatchNormalization()(conv2)
    conv2 = Activation('relu')(conv2)
    conv2 = Dropout(drop)(conv2)
    conv2 = Conv2D(nf, ker, padding="same", kernel_initializer='he_normal', use_bias=False)(conv2)
    ###
    shortcut = Conv2D(nf, 1, padding="same", kernel_initializer='he_normal', use_bias=False)(inputs)
    return add([conv2,shortcut])


# In[ ]:


width = 200 
height = 168
nbClass = 3
nf = 10    # feature dimensions
ker = 3    # kernel dimension

###################################### INPUT LAYER

img_input = Input(shape=(height, width, 1))
######################################ENCODER

conv1 = FirstResUnit(nf,ker,img_input)
pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

conv2 = ResUnit(nf,ker,pool1)
pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

#######################################center

convC = ResUnit(nf,ker,pool2)

#######################################DECODER

up2 = concatenate([UpSampling2D((2,2))(convC), conv2])
decod2 = ResUnitDecoder(nf,ker,up2)

up1 = concatenate([UpSampling2D((2,2))(decod2), conv1])
decod1 = ResUnitDecoder(nf,ker,up1)

####################################### Segmentation Layer

x = Conv2D(nbClass, (1, 1), padding="valid", use_bias=False)(decod1) 
x = Reshape((height * width, nbClass))(x) 
x = Activation("softmax")(x)
eddynet = Model(img_input, x)

eddynet.summary()


# ### calculate weights (some heavy data imbalance out there...)

# In[ ]:


unique, counts = np.unique(Seg_train, return_counts=True)
dict(zip(unique, counts))


# In[ ]:


freq = [np.sum(counts)/j for j in counts]
weightsSeg = [f/np.sum(freq) for f in freq]
weightsSeg


# ### loss functions

# In[7]:


smooth = 1.  # to avoid zero division

def dice_coef_anti(y_true, y_pred):
    y_true_anti = y_true[:,:,1]
    y_pred_anti = y_pred[:,:,1]
    intersection_anti = K.sum(y_true_anti * y_pred_anti)
    return (2 * intersection_anti + smooth) / (K.sum(y_true_anti)+ K.sum(y_pred_anti) + smooth)

def dice_coef_cyc(y_true, y_pred):
    y_true_cyc = y_true[:,:,2]
    y_pred_cyc = y_pred[:,:,2]
    intersection_cyc = K.sum(y_true_cyc * y_pred_cyc)
    return (2 * intersection_cyc + smooth) / (K.sum(y_true_cyc) + K.sum(y_pred_cyc) + smooth)

def dice_coef_nn(y_true, y_pred):
    y_true_nn = y_true[:,:,0]
    y_pred_nn = y_pred[:,:,0]
    intersection_nn = K.sum(y_true_nn * y_pred_nn)
    return (2 * intersection_nn + smooth) / (K.sum(y_true_nn) + K.sum(y_pred_nn) + smooth)
    
def mean_dice_coef(y_true, y_pred):
    return (dice_coef_anti(y_true, y_pred) + dice_coef_cyc(y_true, y_pred) + dice_coef_nn(y_true, y_pred))/3.

def weighted_mean_dice_coef(y_true, y_pred):
    return (weightsSeg[1]*dice_coef_anti(y_true, y_pred) +  weightsSeg[2]*dice_coef_cyc(y_true, y_pred) + weightsSeg[0]*dice_coef_nn(y_true, y_pred))
  
def dice_coef_loss(y_true, y_pred):
    return 1 - weighted_mean_dice_coef(y_true, y_pred)


# ### Compile Model

# In[8]:


eddynet.compile(optimizer=Adam(learning_rate=1e-3), loss=dice_coef_loss,
                metrics=['categorical_accuracy', mean_dice_coef, weighted_mean_dice_coef])


# In[ ]:


#SVG(model_to_dot(eddynet).create(prog='dot', format='svg'))


# ### Training
# 

# In[ ]:


earl = EarlyStopping(monitor='val_loss', min_delta=1e-8, patience=50, verbose=1, mode='auto')
modelcheck = ModelCheckpoint('/home1/datawork/ddasilva/data/reseau_neurone/eddyRESnet_E2.h5', monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=True)
reducecall = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=20, verbose=1, mode='auto', min_delta=1e-30, min_lr=1e-30)


histeddynet=eddynet.fit(SSH_train,   #x
                        Seg_train_categor, # y
                               epochs=50, #is one full feed forward and backward
                              batch_size=50, #  devide the date in batchs
                              shuffle=True,
                              verbose=1, # what you want to see on the screen while the weights are been updated
                              callbacks=[modelcheck,reducecall, earl],
                              validation_split=0.2 # % of data that you keep asside for validation porpuses
                              )


# In[ ]:


plt.figure(figsize=(10, 10))
plt.semilogy(eddynet.history.history['loss'])
plt.semilogy(eddynet.history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train_loss', 'val_loss'], loc='upper right');


# ### Performance on train dataset

# In[ ]:


randindex=np.random.randint(0,len(SSH_train))
predictedSEGM=eddynet.predict(np.reshape(SSH_train[randindex,:,:],(1,height,width,1)))
predictedSEGMimage = np.reshape(predictedSEGM.argmax(2),(height,width))

plt.figure(figsize=(20, 10))

plt.subplot(131)
plt.imshow(SSH_train[randindex,:,:,0], cmap='viridis')
plt.colorbar(extend='both', fraction=0.042, pad=0.04)
#plt.clim(-0.25,0.25)
plt.axis('off')
plt.title('SLA');

plt.subplot(132)
plt.imshow(predictedSEGMimage, cmap='viridis')
plt.colorbar(extend='both', fraction=0.042, pad=0.04)
#plt.clim(-0.25,0.25)
plt.axis('off')
plt.title('Eddynet Segmentation');

plt.subplot(133)
plt.imshow(Seg_train[randindex,:,:,0], cmap='viridis')
plt.colorbar(extend='both', fraction=0.042, pad=0.04)
#plt.clim(-0.25,0.25)
plt.axis('off')
plt.title('ground truth Segmentation');


# ### Performance on test dataset 

# In[ ]:


randindex=np.random.randint(0,len(SSH_test))
predictedSEGM=eddynet.predict(np.reshape(SSH_test[randindex,:,:],(1,height,width,1)))
predictedSEGMimage = np.reshape(predictedSEGM.argmax(2),(height,width))

plt.figure(figsize=(20, 10))

plt.subplot(131)
plt.imshow(SSH_test[randindex,:,:,0], cmap='viridis')
plt.colorbar(extend='both', fraction=0.042, pad=0.04)
#plt.clim(-0.25,0.25)
plt.axis('off')
plt.title('SLA');

plt.subplot(132)
plt.imshow(predictedSEGMimage, cmap='viridis')
plt.colorbar(extend='both', fraction=0.042, pad=0.04)
#plt.clim(-0.25,0.25)
plt.axis('off')
plt.title('Eddynet Segmentation');

plt.subplot(133)
plt.imshow(Seg_test[randindex,:,:,0], cmap='viridis')
plt.colorbar(extend='both', fraction=0.042, pad=0.04)
#plt.clim(-0.25,0.25)
plt.axis('off')
plt.title('ground truth Segmentation');


# ### metriques on test dataset
# 

# In[ ]:


Seg_test_categor = np_utils.to_categorical(np.reshape(Seg_test[:,:,:,0],(365,168*200)),3)
Seg_test_categor.shape


# In[ ]:


eddynet.evaluate(SSH_test,Seg_test_categor)


# # Analyse  

# In[15]:


#load points
eddynet.load_weights('/home1/datawork/ddasilva/data/reseau_neurone/eddyRESnet_E2.h5')


# In[16]:


# prediction total dans une list
prediction= []
for i in range(SSH_test.shape[0]):
    predictedSEGM=eddynet.predict(np.reshape(SSH_test[i,:,:],(1,height,width,1)))
    predictedSEGMimage = np.reshape(predictedSEGM.argmax(2),(height,width))
    prediction.append(predictedSEGMimage)
#    test=Seg_test[randindex,:,:,0]


# In[ ]:


predictedSEGMimage


# In[17]:


#flatten
prediction2=np.asarray(prediction).flatten()
Seg_test2=Seg_test.flatten()


# # matrix confusion 

# In[18]:


from sklearn.metrics import confusion_matrix
import sklearn.metrics as metrics


# In[19]:


# Matrix confusion
matrix = metrics.confusion_matrix(Seg_test2,prediction2)


# In[ ]:


matrix


# In[ ]:


import seaborn as sns

ax = sns.heatmap(matrix, annot=True, cmap='Blues')

ax.set_title('Seaborn Confusion Matrix with labels\n\n');
ax.set_xlabel('\nPredicted Values')
ax.set_ylabel('Actual Values ');

## Ticket labels - List must be in alphabetical order
ax.xaxis.set_ticklabels(['0','1','2'])
ax.yaxis.set_ticklabels(['0','1','2'])

## Display the visualization of the Confusion Matrix.


# In[ ]:


ax = sns.heatmap(matrix/np.sum(matrix), annot=True, 
            fmt='.2%', cmap='Blues')

ax.set_title('Seaborn Confusion Matrix with labels\n\n');
ax.set_xlabel('\nValeurs prédites')
ax.set_ylabel('Valeurs réelles ');

## Ticket labels - List must be in alphabetical order
ax.xaxis.set_ticklabels(['Vide','Anticyclone','Cyclone'])
ax.yaxis.set_ticklabels(['Vide','Anticyclone','Cyclone'])


## Display the visualization of the Confusion Matrix.
plt.show()


# # metriques 

# In[ ]:


# Accuracy
from sklearn.metrics import accuracy_score
accuracy=accuracy_score(Seg_test2, prediction2)#  
accuracy


# In[ ]:


#recall
from sklearn.metrics import recall_score
recall=recall_score(Seg_test2, prediction2, average=None)#  
recall


# In[ ]:



from sklearn.metrics import precision_score
precision=precision_score(Seg_test2, prediction2, average=None)
precision


# In[ ]:


# Method 2: Manual Calculation
F1 = 2 * (precision * recall) / (precision + recall)
F1

