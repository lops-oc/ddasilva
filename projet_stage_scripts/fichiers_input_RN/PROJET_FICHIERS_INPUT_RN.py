#!/usr/bin/env python
# coding: utf-8

# # Générer les fichiers inputs pour la réseau de neurone

# ### Import library

import intake
import dask_hpcconfig
from distributed import Client
import xarray as xr
import hvplot.xarray
import geoviews.feature as gf
import os
import glob
import os
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats 
import seaborn as sns
import numpy as np
import matplotlib
from py_eddy_tracker.poly import create_vertice

# cartopy
from cartopy import config
import cartopy.crs as ccrs

# eddy tracker
from datetime import datetime
from matplotlib import pyplot as plt
from py_eddy_tracker import data
from py_eddy_tracker.dataset.grid import RegularGridDataset

from py_eddy_tracker import data
from py_eddy_tracker.observations.observation import EddiesObservations
from matplotlib import pyplot as plt
from numpy import arange, cos, linspace, radians, sin
from py_eddy_tracker.eddy_feature import Contours
from py_eddy_tracker.generic import local_to_coordinates
from datetime import timedelta, date
import os, glob


# # Dask

 # overrides = {"cluster.cores": 7,"cluster.n_workers":7,"cluster.processes":7}
 # cluster = dask_hpcconfig.cluster("datarmor", **overrides)
 # cluster.scale(jobs=4)
 # client = Client(cluster)

 # client


# # Paramètres

period='2010_2021'
region='MENOR'


# open file

ds = xr.open_dataset("/home1/scratch/ddasilva/FICHIERS_"+region+"_"+period+".nc" )


# # Fichier train (aprox 85%)

XE_train= ds.XE.isel(ni=slice(300,500),nj=slice(0,200)).sel(time=(slice('2010','2019')))
masque_train= ds.masque.isel(ni=slice(300,500),nj=slice(0,200)).sel(time=(slice('2010','2019')))

#sauvegarder np
XE_train_np = XE_train.to_numpy()
np.save("/home1/scratch/ddasilva/XE_TRAIN_MENOR_2010_2019", arr=XE_train_np)

masque_train_np = masque_train.to_numpy()
np.save("/home1/scratch/ddasilva/MASQUE_TRAIN_MENOR_2010_2019", arr=masque_train_np)


# # Fichier test (aprox 15%)

XE_test= ds.XE.isel(ni=slice(300,500),nj=slice(0,200)).sel(time=(slice('2020','2021')))
masque_test= ds.masque.isel(ni=slice(300,500),nj=slice(0,200)).sel(time=(slice('2020','2021')))

#sauvegarder np
XE_test_np = XE_test.to_numpy()
np.save("/home1/scratch/ddasilva/XE_TEST_MENOR_2020_2021", arr=XE_test_np)

masque_test_np = masque_test.to_numpy()
np.save("/home1/scratch/ddasilva/MASQUE_TEST_MENOR_2020_2021", arr=masque_test_np)

