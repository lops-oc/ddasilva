#!/usr/bin/env python
# coding: utf-8

# In[1]:


import intake
import dask_hpcconfig
from distributed import Client
import xarray as xr
import hvplot.xarray
#import geoviews.feature as gf
import os
import glob
import os
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import seaborn as sns
import numpy as np

# cartopy
from cartopy import config
import cartopy.crs as ccrs

# eddy tracker
from datetime import datetime
from matplotlib import pyplot as plt
#from py_eddy_tracker import data
#from py_eddy_tracker.dataset.grid import RegularGridDataset


# In[2]:


overrides = {"cluster.cores": 7,"cluster.n_workers":7,"cluster.processes":7}
cluster = dask_hpcconfig.cluster("datarmor", **overrides)
cluster.scale(jobs=4)
client = Client(cluster)
client


# In[3]:


client


# In[2]:


def list_param(cat, param):
    return cat.metadata["parameters"][param]["allowed"]

def allowed_param(cat):
    return cat.metadata["parameters"]

def allowed_years(data):
    path = "/home/datawork-lops-iaocea/catalog/kerchunk/ref-marc/" + data
    files = [f.removesuffix(".json.zst") for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    files.sort()
    
    return files

def year_concat(cat, source, region, years,chunks):
    concat = [cat[source](region=region, year=i,chunks=chunks).to_dask() 
              for i in years]

    return xr.concat(
        concat, 
        dim="time", 
        coords="minimal", 
        data_vars="minimal", 
        compat="override"
    )



# #  Parameters

# In[3]:


period='2010_2021'
produit='MENOR'


# In[4]:


cat = intake.open_catalog("/home/datawork-lops-iaocea/catalog/intake/marc.yaml")


# In[5]:


#region = 'f1_e2500'   #mangea
region = 'f2_1200_sn'  #menor -mediteranne
year = allowed_years(data=region)
year[0:-1]

 # ds total
get_ipython().run_line_magic('time', '')
ds = year_concat(cat, "marc", region,year[0:-1],chunks={ "nj": -1 ,"ni": -1})

regions = list_param(
    cat, "region"
)

varname = "TEMP"

region = 'f2_1200'  #menor -mediteranne - 2010 a 2013
region2 = 'f2_1200_v10'  #menor -mediteranne 2013 et 14
region3 = 'f2_1200_sn'  #menor -mediteranne 2015-2022

year = allowed_years(data=region)
year2 = allowed_years(data=region2)
year3 = allowed_years(data=region3)

year_select=year
year_select2=year2[0:-1]
year_select3=year3[0:-1]

# ds total
get_ipython().run_line_magic('time', '')
ds1 = year_concat(cat, "marc", region,year_select,chunks={ "nj": -1 ,"ni": -1})
ds2 = year_concat(cat, "marc", region2,year_select2,chunks={ "nj": -1 ,"ni": -1})
ds3 = year_concat(cat, "marc", region3,year_select3,chunks={ "nj": -1 ,"ni": -1})

ds1=ds1.sel(time=(slice('2010-01-01T00:00:00.000000000','2013-08-30T21:00:00.000000000')))

ds_monthly  =  ds1[varname].isel(level=-1)
ds_monthly2 =  ds2[varname].isel(level=-1)
ds_monthly3 =  ds3[varname].isel(level=-1)
ds = xr.concat([ds_monthly,ds_monthly2,ds_monthly3],'time')


# In[ ]:


# sauvegarder climatologie 
ds_clim = ds.groupby("time.month").mean(dim="time")  
path="/home1/datawork/ddasilva/data/climat/CLIMAT_SST_"+produit+"_"+period+".nc"
ds_clim.to_netcdf(path=path) 


# In[ ]:


#anomalie de SST 
#path="/home1/datawork/ddasilva/data/climat/CLIMAT_SST_"+produit+"_"+period+".nc" 
#ds_clim = xr.open_dataset(path)
#ds_monthly = ds.resample(time="1M").mean(dim="time")
#ano = ds_monthly.groupby("time.month") - ds_clim


# In[ ]:


# Serie temporelle de SST par dimension 'time','ni'et 'nj'  

temp_moyenne_tij = ds.resample(time="1M").mean(dim=('time','ni','nj'))  
df_temp_moyenne_tij=temp_moyenne_tij.to_dataframe().reset_index()

# nouvelles variables
df_temp_moyenne_tij['mois'] = df_temp_moyenne_tij['time'].dt.month 
df_temp_moyenne_tij['annee'] = df_temp_moyenne_tij['time'].dt.year
df_temp_moyenne_tij['annee_mois'] =df_temp_moyenne_tij["time"].dt.to_period('M').dt.strftime('%Y-%m')
df_temp_moyenne_tij=df_temp_moyenne_tij.rename(columns = {'TEMP':'temp_moyenne'})
#climatologie : moyenne par mois
climat=df_temp_moyenne_tij.groupby('mois').mean().reset_index().rename(columns = {'temp_moyenne':'climatomogie'})

result2=pd.merge(df_temp_moyenne_tij,climat, how ='left', left_on="mois" , right_on = "mois")

result2['temp_moyenne'] =result2['temp_moyenne'].replace('',pd.NA).fillna(result2['climatomogie'])
result2['anomalie']=  result2['temp_moyenne'] - result2['climatomogie']
result2=result2.sort_values(by=['time'])
result2['labels']=""
result2['labels'][np.arange(0,144,12)]=result2['annee_mois'][np.arange(0,144,12)]

# sauvegarder serie temporelle
from pathlib import Path  
filepath = Path("/home1/datawork/ddasilva/data/sst/SST_"+produit+"/SST_"+produit+"_"+period+".csv")  
result2.to_csv(filepath)  


#plot temperature, climatologie et anomalie

plt.figure(figsize=(80,20))
plt.plot( 'annee_mois', 'temp_moyenne', data=result2, markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4,label = "température moyenne")
plt.plot( 'annee_mois', 'climatomogie', data=result2, color='olive', linewidth=2,label = "climatologie")
   
plt.xticks(result2['labels'],rotation=50, fontsize=30,horizontalalignment="center") 


#plt.title("Température et climatologie mensuelles moyennes dans la mer Méditerranée Nord-Occidentale" , fontsize=40)
plt.xlabel("Mois", fontsize=30, fontweight='black', color = '#333F4B')
plt.ylabel("SST (C°)", fontsize=30, fontweight='black', color = '#333F4B')

# show legend
plt.legend(fontsize=30)
plt.savefig("/home1/datawork/ddasilva/data/sst/SST_"+produit+"/SST_"+produit+"_"+period+".png", dpi=70,  
            facecolor="white", edgecolor='none',transparent='false')


# Tendance
from scipy import stats
result3=result2

x = result3.index
y= result3['anomalie']

plt.figure(figsize=(80,20))
plt.plot(x, y, color='grey')

# linear regression
res = stats.linregress(x, y)

# coefficient of determination (R-squared)
print(f"R-squared: {res.rvalue**2:.6f}")
plt.plot(x, y,color='grey')
inters= res.intercept
inters= res.intercept


plt.legend()
plt.xlabel('Année',fontsize=20)
plt.ylabel('SST (C°)',fontsize=20)
#plt.title(' SST anomalie du Golfe de Gascogne',fontsize=40)
plt.savefig("/home1/datawork/ddasilva/data/sst/SST_"+produit+"/TENDANCE_SST_"+produit+"_"+period+".png")

plt.plot(x, res.intercept + res.slope*x,'r', label='fitted line')

plt.show()
# show legend
plt.legend(fontsize=30)

# Régression linéaire 
from scipy.stats import t

tinv = lambda p, df: abs(t.ppf(p/2, df))
ts = tinv(0.05, len(x)-2)

print(f"slope (95%): {res.slope:.4f} +/- {ts*res.stderr:.4f}")
print(f"intercept (95%): {res.intercept:.4f}"
 f" +/- {ts*res.intercept_stderr:.4f}")



