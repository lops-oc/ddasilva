#!/usr/bin/env python
# coding: utf-8

# In[3]:


import intake
import dask_hpcconfig
from distributed import Client
import xarray as xr
import hvplot.xarray
#import geoviews.feature as gf
import os
import glob
import os
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import seaborn as sns
import numpy as np

# cartopy
from cartopy import config
import cartopy.crs as ccrs

# eddy tracker
from datetime import datetime
from matplotlib import pyplot as plt
#from py_eddy_tracker import data
#from py_eddy_tracker.dataset.grid import RegularGridDataset


# In[4]:


overrides = {"cluster.cores": 7,"cluster.n_workers":7,"cluster.processes":7}
cluster = dask_hpcconfig.cluster("datarmor", **overrides)
cluster.scale(jobs=4)
client = Client(cluster)
client


# In[6]:


client


# In[7]:


def list_param(cat, param):
    return cat.metadata["parameters"][param]["allowed"]

def allowed_param(cat):
    return cat.metadata["parameters"]

def allowed_years(data):
    path = "/home/datawork-lops-iaocea/catalog/kerchunk/ref-marc/" + data
    files = [f.removesuffix(".json.zst") for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    files.sort()
    
    return files

def year_concat(cat, source, region, years,chunks):
    concat = [cat[source](region=region, year=i,chunks=chunks).to_dask() 
              for i in years]

    return xr.concat(
        concat, 
        dim="time", 
        coords="minimal", 
        data_vars="minimal", 
        compat="override"
    )


# #  Paramètres

# In[22]:


cat = intake.open_catalog("/home/datawork-lops-iaocea/catalog/intake/marc.yaml")
regions = list_param(cat, "region")
varname = "TEMP"
region = 'f1_e2500'   #mangea
year = allowed_years(data=region)

period='2006_2021'
produit='GASCOGNE'


# In[9]:


# ds total
get_ipython().run_line_magic('time', '')
ds = year_concat(cat, "marc", region,year[0:-1],chunks={ "nj": -1 ,"ni": -1})
ds =  ds[varname].isel(level=-1)
ds


# # Climatologie 

# In[7]:


ds_clim = ds.groupby("time.month").mean(dim="time")  
ds_clim


# In[ ]:


path="/home1/datawork/ddasilva/data/climat/CLIMAT_SST_"+produit+"_"+period+".nc"
ds_clim.to_netcdf(path=path) 


# In[ ]:


path="/home1/datawork/ddasilva/data/climat/CLIMAT_SST_"+produit+"_"+period+".nc" 
ds_clim = xr.open_dataset(path)


# # Anomalie SST 

# In[ ]:


ds_monthly = ds.resample(time="1M").mean(dim="time") # Temperature moyenne
ano = ds_monthly.groupby("time.month") - ds_clim    # anoamalie
ano = ds.groupby("time.month") - ds_clim
ano


# # Serie temporelle de SST par dimension 'time','ni'et 'nj'  

# In[10]:


temp_moyenne_tij = ds.resample(time="1M").mean(dim=('time','ni','nj'))  
df_temp_moyenne_tij=temp_moyenne_tij.to_dataframe().reset_index()

# nouvelles variables
df_temp_moyenne_tij['mois'] = df_temp_moyenne_tij['time'].dt.month 
df_temp_moyenne_tij['annee'] = df_temp_moyenne_tij['time'].dt.year
df_temp_moyenne_tij['annee_mois'] =df_temp_moyenne_tij["time"].dt.to_period('M').dt.strftime('%Y-%m')
df_temp_moyenne_tij=df_temp_moyenne_tij.rename(columns = {'TEMP':'temp_moyenne'})
#df_temp_moyenne_tij


# In[11]:


#climatologie : moyenne par mois
climat=df_temp_moyenne_tij.groupby('mois').mean().reset_index().rename(columns = {'temp_moyenne':'climatomogie'})
result2=pd.merge(df_temp_moyenne_tij,climat, how ='left', left_on="mois" , right_on = "mois")

result2['temp_moyenne'] =result2['temp_moyenne'].replace('',pd.NA).fillna(result2['climatomogie'])
result2['anomalie']=  result2['temp_moyenne'] - result2['climatomogie']
result2=result2.sort_values(by=['time'])
result2


# LABEL: Attention. Changer !!! 


result2['labels'][np.arange(0,191,12)]=result2['annee_mois'][np.arange(0,191,12)]


# In[19]:


result2['labels']


# In[ ]:


labels


# In[23]:


from pathlib import Path  
filepath = Path("/home1/datawork/ddasilva/data/sst/SST_"+produit+"/SST_"+produit+"_"+period+".csv")  
result2.to_csv(filepath)  


# # Plot 

# In[24]:


#plot temperature, climatologie et anomalie

plt.figure(figsize=(80,20))
plt.plot( 'annee_mois', 'temp_moyenne', data=result2, markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4,label = "température moyenne")
plt.plot( 'annee_mois', 'climatomogie', data=result2, color='olive', linewidth=2,label = "climatologie")
   
plt.xticks(result2['labels'],rotation=50, fontsize=30,horizontalalignment="center") 


#plt.title("Température et climatologie mensuelles moyennes dans la mer Méditerranée Nord-Occidentale" , fontsize=40)
plt.xlabel("Mois", fontsize=30, fontweight='black', color = '#333F4B')
plt.ylabel("SST (C°)", fontsize=30, fontweight='black', color = '#333F4B')


# show legend
plt.legend(fontsize=30)
plt.savefig("/home1/datawork/ddasilva/data/sst/SST_"+produit+"/SST_"+produit+"_"+period+".png", dpi=70,  facecolor="white", edgecolor='none',transparent='false')


# #  Tendance

# In[30]:


from scipy import stats
result3=result2

x = result3.index
y= result3['anomalie']

plt.figure(figsize=(80,20))
plt.plot(x, y, color='grey')

# linear regression
res = stats.linregress(x, y)

# coefficient of determination (R-squared)
print(f"R-squared: {res.rvalue**2:.6f}")
plt.plot(x, y,color='grey')
inters= res.intercept
inters= res.intercept


plt.legend()
plt.xlabel('Année',fontsize=20)
plt.ylabel('SST (C°)',fontsize=20)
#plt.title(' SST anomalie du Golfe de Gascogne',fontsize=40)
plt.savefig("/home1/datawork/ddasilva/data/sst/SST_"+produit+"/TENDANCE_SST_"+produit+"_"+period+".png")

plt.plot(x, res.intercept + res.slope*x,'r', label='fitted line')

plt.show()
# show legend
plt.legend(fontsize=30)


# In[12]:


# Two-sided inverse Students t-distribution
# p - probability, df - degrees of freedom
from scipy.stats import t

tinv = lambda p, df: abs(t.ppf(p/2, df))


# In[16]:


ts = tinv(0.05, len(x)-2)

print(f"slope (95%): {res.slope:.4f} +/- {ts*res.stderr:.4f}")
print(f"intercept (95%): {res.intercept:.4f}"
 f" +/- {ts*res.intercept_stderr:.4f}")


# In[ ]:




